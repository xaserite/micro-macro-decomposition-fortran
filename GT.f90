module Goldstein_Taylor
	implicit none
	private

	type,public :: GT
		real(8),dimension(:),allocatable :: x
		real(8),dimension(2) :: V,w,E,f_L,f_R
		real(8),dimension(:,:),allocatable :: f_0
		real(8) :: T, eps
		integer :: j, x_N, t_N, v_N
	contains
		procedure :: setup => GT_setup
		procedure :: free => GT_free
		procedure :: toFile => GT_toFile
		procedure :: toFile_MATLAB => GT_toFile_MATLAB
	end type GT
contains
	subroutine GT_setup(this,x_N,t_N,T,eps,f_L_a,f_L_b,f_R_a,f_R_b,x,f_0)
		class(GT):: this
		real(8),dimension(:) :: x,f_0
		real(8) :: T, eps, f_L_a,f_L_b,f_R_a,f_R_b
		integer :: x_N, t_N
		allocate(this%x(x_N))
		allocate(this%f_0(x_N,2))
		this%x_N = x_N
		this%t_N = t_N
		this%v_N = 2
		this%T = T
		this%eps = eps
		this%V(1)=1
		this%V(2)=-1
		this%E(1)=1
		this%E(2)=1
		this%w(1)=.5
		this%w(2)=.5
		this%f_L(1)=f_L_a
		this%f_L(2)=f_L_b
		this%f_R(1)=f_R_a
		this%f_R(2)=f_R_b
		this%x = x
		this%f_0(:,1)=f_0
		this%f_0(:,2)=f_0
	end subroutine
	subroutine GT_free(this)
		class(GT):: this
		if(allocated(this%x)) then
			deallocate(this%x)
		end if
		if(allocated(this%f_0)) then
			deallocate(this%f_0)
		end if
	end subroutine
	subroutine GT_toFile(this,FILENAME)
		class(GT):: this
		character(len=*):: FILENAME
		integer :: j
		open(unit=1,file=FILENAME)
			write(1,*) this%x_N, this%t_N, this%v_N
			write(1,*) this%T, this%eps
			write(1,*) this%x
			write(1,*) this%w
			write(1,*) this%V
			write(1,*) this%E
			write(1,*) this%f_L
			write(1,*) this%f_R
			do j=1,this%v_N
				write(1,*) this%f_0(:,j)
			end do
		close(1)
	end subroutine
	subroutine GT_toFile_MATLAB(this,FILENAME)
		class(GT):: this
		character(len=*):: FILENAME
		integer :: j
		open(unit=11,file=FILENAME)
			write(11,*) this%x_N, this%t_N
			write(11,*) this%T, this%eps
			write(11,*) this%x
			do j=1,this%v_N
				write(11,*) this%f_0(:,j)
			end do
		close(11)
	end subroutine
end module
