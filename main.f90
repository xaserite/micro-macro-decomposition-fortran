program MMD_test
	use module_micro_macro
	use Goldstein_Taylor
	implicit none
	type(MMD) :: M
	type(GT)	:: G
	character(len=*),parameter :: FILE_PARAM= '../param.data', &
		FILE_IN='problem.data', FILE_OUT='rho.data'
	integer :: x_N=100,t_N=400
	real(8) :: T,eps,f_L_a,f_L_b=0,f_R_a=0,f_R_b=0
	real(8),dimension(:),allocatable :: x, f_0
	integer :: j
	real(8) :: dx
	! read params
	open(unit=99,file=FILE_PARAM)
		read(99,*) x_N, t_N
		read(99,*) T, eps
		read(99,*) f_L_a,f_R_b
	close(99)
	! set initial data
	allocate(x(x_N+1))
	allocate(f_0(x_N+1))
	dx = 2./x_N
	do j=1,x_N+1
		x(j) = (j-1)*dx-1.
		if((x(j)<.2) .AND. (x(j)>-.2)) then
			f_0(j)=1
		else
			f_0(j)=0
		end if
	end do
	! write problem
	call G%setup(x_N+1,t_N,T,eps,f_L_a,f_L_b,f_R_a,f_R_b,x,f_0)
	call G%toFile(FILE_IN)
	call G%free
	deallocate(x)
	deallocate(f_0)
	! solve
	call M%fromFile(FILE_IN)
	call M%compute
	call M%to_Gnuplot(FILE_OUT)
end program
