module module_micro_macro
implicit none
private

type, public :: MMD
		real(8),allocatable,dimension(:,:)	:: ro   ! equilibrium part (micro) function array | dependent on (t,x)
		real(8),allocatable,dimension(:,:,:):: g 	! scaled part (macro) function array |dependent on (t,x,v)
		real(8),allocatable,dimension(:)	:: V 	! velocity space | discretized
		real(8),allocatable,dimension(:)	:: w 	! integral weight vector according to measure in velocity space | eg. Gaussian quadrature rule
		real(8),allocatable,dimension(:)	:: E 	! absolute equilibrium state on V
		real(8),allocatable,dimension(:)	:: x 	! uniform spatial grid point vector | may internally only be needed for explicit function calls gamma(x)
		real(8),allocatable,dimension(:,:)	:: f_0  ! IC | dependent on (x,v)
		real(8),allocatable,dimension(:,:)	:: f_T  ! solution | dependent on (x,v)
		real(8),allocatable,dimension(:)	:: f_L, f_R ! BC | dependent on (v)
		real(8),allocatable,dimension(:)	:: Dg   ! forward/backward space derivative of g | dependant on (v)
		real(8) :: T, dx, dt 		! discretization parameters
		integer :: x_N,t_N,t_k,v_N  ! discretization parameters
		real(8) :: eps  			! diffusive scaling parameter
		real(8) :: vIntp, vIntm		! int[v^+ dw(v)], int[v^- dw(v)]
	contains
		procedure :: init => MMD_init
		procedure :: free => MMD_free
		procedure :: fromFile => MMD_from_file
		procedure :: toFile => MMD_to_file
		procedure :: set_eps => MMD_set_eps
		procedure :: compute => MMD_compute
		procedure :: solution => MMD_solution_to_file
		procedure :: to_Gnuplot => MMD_toGnuplot
end type MMD

contains
	!
	! class creators
  subroutine MMD_init(this,f_0,V,w,E,x,f_L,f_R,T,t_N,eps)
    class(MMD) :: this
		real(8),dimension(:),intent(in) ::  x,w,V,E, f_L, f_R
		real(8),dimension(:,:),intent(in) :: f_0
		real(8),intent(in) :: T, eps
		integer,intent(in) :: t_N
		this%x_N = size(x)
		this%v_N = size(V)
		this%E = E
		this%V = V
		this%w = w
		this%x = x
		this%f_0 = f_0
		this%f_L = f_L
		this%f_R = f_R
		this%T = T
		this%t_N = t_N
		this%eps = eps
		this%t_k = 1
		this%dt = T/t_N
		this%dx = x(2) - x(1)
		call MMD_init_functions(this)
		call MMD_init_IC(this)
		call MMD_init_vInt(this)
  end subroutine
	! inialize function arrays
	subroutine MMD_init_functions(this)
		class(MMD):: this
		allocate(this%ro(this%t_N,this%x_N))
		allocate(this%g(this%t_N,this%x_N+1,this%v_N))
		allocate(this%Dg(this%v_N))
	end subroutine
	! decompose IC into micro-macro
	! ro lives on natural grid | g lives on staggered grid
	subroutine MMD_init_IC(this)
		class(MMD) :: this
		integer :: i,j
		this%ro(1,:) = integral(this,this%f_0)
		do j=1,this%v_N
			this%g(1,1,j) = (  this%f_0(1,j) - this%ro(1,1)*this%E(j) )/this%eps
			do i=2,this%x_N+1
				this%g(1,i,j) = 2./this%eps * (  this%f_0(i,j) &
						- this%ro(1,i)*this%E(j) ) - this%g(1,i-1,j)
			end do
		end do
	end subroutine
	subroutine MMD_init_vInt(this)
		class(MMD):: this
		integer :: j
		real(8),dimension(this%v_N) :: one
		do j=1,this%v_N
			if(this%V(j)>0) then
				one(j) = 1
			else
				one(j) = 0
			end if
		end do
		this%vIntp = integral_density(this,one)
		do j=1,this%v_N
			if(this%V(j)<0) then
				one(j) = 1
			else
				one(j) = 0
			end if
		end do
		this%vIntm = integral_density(this,one)
	end subroutine
	!
	! change eps to value and reinitialize IC
	subroutine MMD_set_eps(this,eps)
		class(MMD):: this
		real(8)::eps
		this%eps= eps
		call MMD_init_IC(this)
	end subroutine
	!
	! compute
	subroutine MMD_compute(this)
		class(MMD):: this
		integer :: i
		this%t_k=1
		do while(this%t_k<this%t_N)
			do i=2,this%x_N
				call compute_g_inner(this,i)
			end do
			do i=2,this%x_N-1
				call compute_ro_inner(this,i)
			end do
			!call compute_ro_outer_L(this)
			!call compute_ro_outer_R(this)
			!call compute_g_outer(this)
			call compute_outer_Neumann(this)
			this%t_k = this%t_k+1
		end do
	end subroutine
	!
	! compute g(t_k+1,x_{i-1/2},)
	subroutine compute_g_inner(this,i)
		class(MMD):: this
		integer :: i,j
		real(8) :: g_, Dg, Dro, EDro, alpha, Id_L, ProjS,  vDgInt, ProjDg
		alpha = this%dt/this%eps
		Id_L = 1.+this%dt/(this%eps*this%eps)
		call compute_Dg_fb(this,i)
		vDgInt = integral_single(this,this%Dg)
		Dro = (this%ro(this%t_k,i)-this%ro(this%t_k,i-1))/this%dx
		do j=1,this%v_N
			EDro = this%V(j)*this%E(j)*Dro/this%eps
			ProjDg = this%Dg(j)-this%E(j)*vDgInt
			ProjS = 0
			g_ = this%g(this%t_k,i,j) + alpha* (- ProjDg + ProjS - EDro)
			this%g(this%t_k+1,i,j) = g_/Id_L
		end do
	end subroutine
	!
	! compute forward/backward Dg(tk,x_{i-1/2},) - depending on sign of velocity v -
	!						multiplied by velocity
	subroutine compute_Dg_fb(this,i)
		class(MMD):: this
		integer :: i,j
		do j=1,this%v_N
			if (this%V(j)>0) then
				this%Dg(j) = this%V(j) * ( this%g(this%t_k,i,j) - this%g(this%t_k,i-1,j) )/this%dx
			else
				this%Dg(j) = this%V(j) * ( this%g(this%t_k,i+1,j) - this%g(this%t_k,i,j) )/this%dx
			end if
		end do
	end subroutine
	!
	! compute ro(t_k+1,x_i)
	subroutine compute_ro_inner(this,i)
		class(MMD):: this
		integer :: i
		real(8) :: DgInt, SInt, ro_
		call compute_Dg(this,i)
		DgInt = integral_density(this,this%Dg)
		SInt = 0
		ro_ = Sint - DgInt
		this%ro(this%t_k+1,i) = this%ro(this%t_k,i) + this%dt * ro_
	end subroutine
	!
	! compute Dg(t_k+1,x_i,)
	subroutine compute_Dg(this,i)
		class(MMD):: this
		integer :: i,j
		do j=1,this%v_N
			this%Dg(j) = ( this%g(this%t_k+1,i+1,j) - this%g(this%t_k+1,i,j) )/this%dx
		end do
	end subroutine
	!
	! compute ro(t_k+1,1) and ro(t_k+1,N)
	subroutine compute_ro_outer_L(this)
		class(MMD):: this
		integer :: j
		real(8) :: ro_, vg_fInt, alpha, SInt
		real(8),dimension(this%v_N) :: vg_f
		do j=1,this%v_N
			if (this%V(j)>0) then
				vg_f(j) = this%g(this%t_k+1,2,j) - 1./this%eps*this%f_L(j)
				vg_f(j) = 2.*this%V(j)/this%dx * vg_f(j)
			else
				vg_f(j) = 0
			end if
		end do
		SInt = 0
		vg_fInt = integral_single(this,vg_f)
		alpha = 1 + 2.*this%dt/(this%eps*this%dx) * this%vIntp
		ro_ = this%ro(this%t_k,1) + this%dt*(Sint - vg_fInt)
		this%ro(this%t_k+1,1) = ro_/alpha
	end subroutine
	subroutine compute_ro_outer_R(this)
		class(MMD):: this
		integer :: j
		real(8) :: ro_, vg_fInt, alpha, SInt
		real(8),dimension(this%v_N) :: vg_f
		do j=1,this%v_N
			if (this%V(j)<0) then
				vg_f(j) = 1./this%eps*this%f_R(j) - this%g(this%t_k+1,this%x_N,j)
				vg_f(j) = 2.*this%V(j)/this%dx * vg_f(j)
			else
				vg_f(j) = 0
			end if
		end do
		SInt = 0
		vg_fInt = integral_single(this,vg_f)
		alpha = 1 - 2.*this%dt/(this%eps*this%dx) * this%vIntm
		ro_ = this%ro(this%t_k,this%x_N) + this%dt*(Sint - vg_fInt)
		this%ro(this%t_k+1,this%x_N) = ro_/alpha
	end subroutine
	!
	! compute ro(t_k+1,1) and ro(t_k+1,N)
	subroutine compute_outer_Neumann(this)
		class(MMD):: this
		integer :: j
		this%ro(this%t_k+1,1) = this%ro(this%t_k+1,2)
		this%ro(this%t_k+1,this%x_N) = this%ro(this%t_k+1,this%x_N-1)
		do j=1,this%v_N
            this%g(this%t_k+1,1,j) = this%g(this%t_k+1,2,j)
            this%g(this%t_k+1,this%x_N,j) = this%g(this%t_k+1,this%x_N-1,j)
        end do
	end subroutine
	!
	! compute g(t_k+1,-1/2,) and g(t_k+1,N+1/2,)
	subroutine compute_g_outer(this)
		class(MMD):: this
		integer :: j
		real(8) :: g_
		do j=1,this%v_N
			if(this%V(j)>0) then
				this%g(this%t_k+1,1,j) = &
					2./this%eps * ( this%f_L(j)-this%ro(this%t_k+1,1) ) &
					- this%g(this%t_k,2,j)
				this%g(this%t_k+1,1,j) = this%g(this%t_k+1,2,j)
			else
				this%g(this%t_k+1,this%x_N+1,j) = &
					2./this%eps * ( this%f_R(j)-this%ro(this%t_k+1,this%x_N) ) &
					- this%g(this%t_k,this%x_N,j)
				this%g(this%t_k+1,this%x_N+1,j) = this%g(this%t_k+1,this%x_N,j)
			end if
		end do
	end subroutine
	!
	! calculate an array in space of integrals I(x)=int[f(x,)] over the velocity space with weights w
	function integral(this,f) result(I)
		class(MMD):: this
		real(8),dimension(:,:),intent(in) :: f ! f(x,v)
		real(8),dimension(size(f,1)):: I ! I(x)
		integer :: l,j,n,m
		n= size(f,1)
		m= size(f,2)
		do l=1,n
			I(l) = 0
			do j=1,m
				I(l) = I(l) + this%w(j)*f(l,j)
			end do
		end do
	end function
	!
	! calculate the integral I=int[f(v)dw(v)] with weights w
	function integral_single(this,f) result(I)
		class(MMD):: this
		real(8),dimension(:),intent(in) :: f ! f(v)
		real(8):: I
		integer :: j
		I = 0
		do j=1,this%v_N
				I = I + this%w(j)*f(j)
		end do
	end function
	!
	! calculate the integral I=int[vf(v)dw(v)] with weights w
	function integral_density(this,f) result(I)
		class(MMD):: this
		real(8),dimension(:),intent(in) :: f ! f(v)
		real(8):: I
		integer :: j
		I = 0
		do j=1,this%v_N
				I = I + this%V(j)*this%w(j)*f(j)
		end do
	end function
	!
	! free routine
	subroutine MMD_free(this)
		class(MMD):: this
		if(allocated(this%ro)) then
			deallocate(this%ro)
		end if
		if(allocated(this%g)) then
			deallocate(this%g)
		end if
		if(allocated(this%V)) then
			deallocate(this%V)
		end if
		if(allocated(this%w)) then
			deallocate(this%w)
		end if
		if(allocated(this%x)) then
			deallocate(this%x)
		end if
		if(allocated(this%f_L)) then
			deallocate(this%f_L)
		end if
		if(allocated(this%f_R)) then
			deallocate(this%f_R)
		end if
	end subroutine
	!
	! read and init from file
	subroutine MMD_from_file(this, FILENAME)
		class(MMD) :: this
		character(len=*):: FILENAME
		real(8),dimension(:),allocatable :: x,V,w,E,f_L,f_R
		real(8),dimension(:,:),allocatable :: f_0
		real(8) :: T, eps
		integer :: j, x_N, t_N, v_N
		open(unit = 3, file = FILENAME, status='old')
		read(3,*) x_N, t_N, v_N
		read(3,*) T, eps
		allocate(x(x_N))
		allocate(V(v_N))
		allocate(w(v_N))
		allocate(E(v_N))
		allocate(f_L(v_N))
		allocate(f_R(v_N))
		allocate(f_0(x_N,v_N))
		read(3,*) x
		read(3,*) w
		read(3,*) V
		read(3,*) E
		read(3,*) f_L
		read(3,*) f_R
		do j=1,v_N
			read(3,*) f_0(:,j)
		end do
		close(3)
		call MMD_init(this,f_0,V,w,E,x,f_L,f_R,T,t_N,eps)
		deallocate(x)
		deallocate(w)
		deallocate(V)
		deallocate(E)
		deallocate(f_L)
		deallocate(f_R)
		deallocate(f_0)
	end subroutine
	! write to file
	subroutine MMD_to_file(this,FILENAME)
		class(MMD):: this
		character(len=*):: FILENAME
		integer :: j
		open(unit = 5, file = FILENAME)
		write(5,*) this%x
		write(5,*) this%ro(this%t_N,:)
		do j=1,this%v_N
			write(5,*) this%g(this%t_N,:,j)
		end do
		close(5)
	end subroutine
	subroutine MMD_solution_to_file(this,FILENAME)
		class(MMD):: this
		character(len=*):: FILENAME
		integer :: i,j
		open(unit = 7, file = FILENAME)
		write(7,*) this%x_N, this%t_N
		write(7,*) this%eps
		write(7,*) this%x
		allocate(this%f_T(this%x_N,this%v_N))
		do j=1,this%v_N
			do i=1,this%x_N
				this%f_T(i,j)= this%ro(this%t_N,i)*this%E(j) + &
						.5*this%eps *( this%g(this%t_N,i,j) + this%g(this%t_N,i+1,j))
			end do
			write(7,*) this%f_T(:,j)
		end do
		deallocate(this%f_T)
		close(7)
	end subroutine
	subroutine MMD_toGnuplot(this,FILENAME)
		class(MMD):: this
		character(len=*):: FILENAME
		integer :: i
		open(unit = 8, file = FILENAME)
		do i=1,this%x_N
            write(8,*) this%x(i), this%ro(this%t_N,i)
        end do
        close(8)
    end subroutine
end module
